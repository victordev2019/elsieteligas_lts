import "./bootstrap";
import Alpine from "alpinejs";
import storageInit from "./localstorage";

window.Alpine = Alpine;

Alpine.start();
// import "./localstorage";
// INICIALIZANDO VARIBLES LOCALSTORAGE
storageInit();
