<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MatanYadaev\EloquentSpatial\SpatialBuilder;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use MatanYadaev\EloquentSpatial\Traits\HasSpatial;

class Department extends Model
{
    use HasFactory;
    use HasSpatial;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = [
        'location' => Point::class,
        'area' => Polygon::class
    ];
    // RELATIOANS
    public function provinces()
    {
        return $this->hasMany(Province::class);
    }
}
