<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    // VAT CODE *******
    // RELATIONS
    public function league()
    {
        return $this->belongsTo(League::class);
    }
    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }
}
