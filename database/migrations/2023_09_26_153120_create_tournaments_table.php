<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->char('gender', 1);
            $table->string('category');
            $table->date('startofseason');
            $table->date('endofseason');
            $table->unsignedBigInteger('league_id');
            $table->unsignedBigInteger('sport_id');
            $table->tinyInteger('status')->default(1);
            $table->boolean('payment')->default(false);

            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('sport_id')->references('id')->on('sports')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tournaments');
    }
};
