<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->integer('players');
            $table->integer('series');
            $table->integer('classified');
            $table->integer('matchesbygroup');
            $table->integer('matchesplayoff');
            $table->integer('final');
            $table->integer('equality');
            $table->integer('winner');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('tournament_id');

            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('settings');
    }
};
