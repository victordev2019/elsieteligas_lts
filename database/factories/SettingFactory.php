<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Setting>
 */
class SettingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'players' => rand(4, 32),
            'series' => rand(2, 6),
            'classified' => rand(1, 4),
            'matchesbygroup' => rand(1, 2),
            'matchesplayoff' => rand(1, 2),
            'final' => rand(1, 2),
            'equality' => rand(1, 2),
            'winner' => rand(2, 3)
        ];
    }
}
