<?php

namespace Database\Factories;

use App\Models\Municipality;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\League>
 */
class LeagueFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $universe = ['Nacional', 'Regional', 'Provincial', 'Local'];
        $name = $this->faker->company();
        $valorAleatorio = uniqid();
        return [
            'name' => $name,
            'slug' => Str::of($name)->slug("-")->limit(255 - mb_strlen($valorAleatorio) - 1, "")->trim("-")->append("-", $valorAleatorio),
            'universe' => $this->faker->randomElement($universe),
            'membership' => $this->faker->companySuffix(),
            'user_id' => User::inRandomOrder()->first()->id,
            'color' => $this->faker->hexColor(),
            'municipality_id' => Municipality::inRandomOrder()->first()->id
        ];
    }
}
