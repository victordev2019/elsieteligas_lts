<?php

namespace Database\Factories;

use App\Models\Sport;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tournament>
 */
class TournamentFactory extends Factory
{
    public $gender = ['M', 'F'];
    public $category = ['Profesional', 'Amateur', 'Senior', 'Juvenil', 'Infantil'];
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $valorAleatorio = uniqid();
        $name = $this->faker->catchPhrase();
        return [
            'name' => $name,
            'slug' => Str::of($name)->slug("-")->limit(255 - mb_strlen($valorAleatorio) - 1, "")->trim("-")->append("-", $valorAleatorio),
            'gender' => $this->faker->randomElement($this->gender),
            'category' => $this->faker->randomElement($this->category),
            'startofseason' => $this->faker->date(),
            'endofseason' => $this->faker->date(),
            'sport_id' => Sport::inRandomOrder()->first()->id,
        ];
    }
}
