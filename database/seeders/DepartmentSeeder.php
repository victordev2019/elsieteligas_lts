<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use MatanYadaev\EloquentSpatial\Objects\LineString;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Department::create([
            'name' => 'Beni',
            'code' => 'BN',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Chuquisaca',
            'code' => 'CH',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Cochabamba',
            'code' => 'CB',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'La Paz',
            'code' => 'LP',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326),
            'area' => new Polygon([
                new LineString([
                    new Point(12.455363273620605, 41.90746728266806),
                    new Point(12.450309991836548, 41.906636872349075),
                    new Point(12.445632219314575, 41.90197359839437),
                    new Point(12.447413206100464, 41.90027269624499),
                    new Point(12.457906007766724, 41.90000118654431),
                    new Point(12.458517551422117, 41.90281205461268),
                    new Point(12.457584142684937, 41.903107507989986),
                    new Point(12.457734346389769, 41.905918239316286),
                    new Point(12.45572805404663, 41.90637337450963),
                    new Point(12.455363273620605, 41.90746728266806),
                ]),
            ]),
        ]);
        Department::create([
            'name' => 'Oruro',
            'code' => 'OR',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Pando',
            'code' => 'PD',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Potosí',
            'code' => 'PT',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Santa Cruz',
            'code' => 'SC',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
        Department::create([
            'name' => 'Tarija',
            'code' => 'TJ',
            'location' => new Point(-16.50523003434107, -68.09421052553053, 4326)
        ]);
    }
}
