<?php

namespace Database\Seeders;

use App\Models\Sport;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Sport::create([
            'name' => 'Futbol'
        ]);
        Sport::create([
            'name' => 'Futbol 7'
        ]);
        Sport::create([
            'name' => 'Futbol 8'
        ]);
        Sport::create([
            'name' => 'Futbol 9'
        ]);
        Sport::create([
            'name' => 'Futsal'
        ]);
    }
}
