<?php

namespace Database\Seeders;

use App\Models\League;
use App\Models\Setting;
use App\Models\Tournament;
use App\Models\Type;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        League::factory(3000)->create()->each(function (League $league) {
            Tournament::factory(rand(1, 4))->create([
                'league_id' => $league->id
            ])->each(function (Tournament $tournament) {
                Setting::factory()->create([
                    'type_id' => Type::inRandomOrder()->first()->id,
                    'tournament_id' => $tournament->id
                ]);
            });
        });
    }
}
