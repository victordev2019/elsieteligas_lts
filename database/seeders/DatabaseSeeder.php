<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'Victor Avalos Torrez',
            'email' => 'tigervat10@gmail.com',
            'password' => bcrypt('password')
        ]);
        \App\Models\User::factory(10)->create();

        $this->call(DepartmentSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(MunicipalitySeeder::class);
        $this->call(SportSeeder::class);
        // $this->call(TypeSeeder::class);
        // $this->call(LeagueSeeder::class);
    }
}
