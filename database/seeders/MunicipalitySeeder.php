<?php

namespace Database\Seeders;

use App\Models\Municipality;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MunicipalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // *********** MUNICIPIOS BENI
        // cercado
        Municipality::create([
            'name' => 'Trinidad',
            'code' => '080101',
            'province_id' => 1
        ]);
        Municipality::create([
            'name' => 'San Javier',
            'code' => '080102',
            'province_id' => 1
        ]);
        // vaca diez
        Municipality::create([
            'name' => 'Riberalta',
            'code' => '080201',
            'province_id' => 2
        ]);
        Municipality::create([
            'name' => 'Guayaramerín',
            'code' => '080202',
            'province_id' => 2
        ]);
        // j. ballivián
        Municipality::create([
            'name' => 'Reyes',
            'code' => '080301',
            'province_id' => 3
        ]);
        Municipality::create([
            'name' => 'San Borja',
            'code' => '080302',
            'province_id' => 3
        ]);
        Municipality::create([
            'name' => 'Santa Rosa',
            'code' => '080303',
            'province_id' => 3
        ]);
        Municipality::create([
            'name' => 'Rurrenabaque',
            'code' => '080304',
            'province_id' => 3
        ]);
        // yacuma
        Municipality::create([
            'name' => 'Santa Ana',
            'code' => '080401',
            'province_id' => 4
        ]);
        Municipality::create([
            'name' => 'Exaltación',
            'code' => '080402',
            'province_id' => 4
        ]);
        // moxos
        Municipality::create([
            'name' => 'San Ignacio',
            'code' => '080501',
            'province_id' => 5
        ]);
        // marban
        Municipality::create([
            'name' => 'Loreto',
            'code' => '080601',
            'province_id' => 6
        ]);
        Municipality::create([
            'name' => 'San Andrés',
            'code' => '080602',
            'province_id' => 6
        ]);
        // mamoré
        Municipality::create([
            'name' => 'San Joaquín',
            'code' => '080701',
            'province_id' => 7
        ]);
        Municipality::create([
            'name' => 'San Ramón',
            'code' => '080702',
            'province_id' => 7
        ]);
        Municipality::create([
            'name' => 'Puerto Siles',
            'code' => '080703',
            'province_id' => 7
        ]);
        // itenez
        Municipality::create([
            'name' => 'Magdalena',
            'code' => '080801',
            'province_id' => 8
        ]);
        Municipality::create([
            'name' => 'Baures',
            'code' => '080802',
            'province_id' => 8
        ]);
        Municipality::create([
            'name' => 'Huacaraje',
            'code' => '080803',
            'province_id' => 8
        ]);
        // *********** MUNICIPIOS CHUQUISACA
        // oropeza
        Municipality::create([
            'name' => 'Sucre',
            'code' => '010101',
            'province_id' => 9
        ]);
        Municipality::create([
            'name' => 'Yotala',
            'code' => '010102',
            'province_id' => 9
        ]);
        Municipality::create([
            'name' => 'Poroma',
            'code' => '010103',
            'province_id' => 9
        ]);
        // azurduy
        Municipality::create([
            'name' => 'Villa Azurduy',
            'code' => '010201',
            'province_id' => 10
        ]);
        Municipality::create([
            'name' => 'Tarvita',
            'code' => '010202',
            'province_id' => 10
        ]);
        // zudañez
        Municipality::create([
            'name' => 'Villa Zudañez',
            'code' => '010301',
            'province_id' => 11
        ]);
        Municipality::create([
            'name' => 'Presto',
            'code' => '010302',
            'province_id' => 11
        ]);
        Municipality::create([
            'name' => 'Villa Mojocoya',
            'code' => '010303',
            'province_id' => 11
        ]);
        Municipality::create([
            'name' => 'Icla',
            'code' => '010304',
            'province_id' => 11
        ]);
        // tomina
        Municipality::create([
            'name' => 'Villa Alcalá',
            'code' => '010404',
            'province_id' => 12
        ]);
        Municipality::create([
            'name' => 'Padilla',
            'code' => '010401',
            'province_id' => 12
        ]);
        Municipality::create([
            'name' => 'Tomina',
            'code' => '010402',
            'province_id' => 12
        ]);
        Municipality::create([
            'name' => 'Sopachuy',
            'code' => '010403',
            'province_id' => 12
        ]);
        Municipality::create([
            'name' => 'El Villar',
            'code' => '010405',
            'province_id' => 12
        ]);
        // hernando siles
        Municipality::create([
            'name' => 'Monteagudo',
            'code' => '010501',
            'province_id' => 13
        ]);
        Municipality::create([
            'name' => 'San Pablo de Huacareta',
            'code' => '010502',
            'province_id' => 13
        ]);
        // yamparaez
        Municipality::create([
            'name' => 'Tarabuco',
            'code' => '010601',
            'province_id' => 14
        ]);
        Municipality::create([
            'name' => 'Yamparáez',
            'code' => '010602',
            'province_id' => 14
        ]);
        // nor cinti
        Municipality::create([
            'name' => 'San Lucas',
            'code' => '010702',
            'province_id' => 15
        ]);
        Municipality::create([
            'name' => 'Camargo',
            'code' => '010701',
            'province_id' => 15
        ]);
        Municipality::create([
            'name' => 'Incahuasi',
            'code' => '010703',
            'province_id' => 15
        ]);
        Municipality::create([
            'name' => 'Villa Charcas',
            'code' => '010704',
            'province_id' => 15
        ]);
        // sud cinti
        Municipality::create([
            'name' => 'Villa Abecia',
            'code' => '010901',
            'province_id' => 16
        ]);
        Municipality::create([
            'name' => 'Culpina',
            'code' => '010902',
            'province_id' => 16
        ]);
        Municipality::create([
            'name' => 'Las Carreras',
            'code' => '010903',
            'province_id' => 16
        ]);
        // belizario boeto
        Municipality::create([
            'name' => 'Villa Serrano',
            'code' => '010801',
            'province_id' => 17
        ]);
        // luis calvo
        Municipality::create([
            'name' => 'Muyupampa',
            'code' => '011001',
            'province_id' => 18
        ]);
        Municipality::create([
            'name' => 'Huacaya',
            'code' => '011002',
            'province_id' => 18
        ]);
        Municipality::create([
            'name' => 'Macharetí',
            'code' => '011003',
            'province_id' => 18
        ]);
        // *********** MUNICIPIOS COCHABAMBA
        // arani
        Municipality::create([
            'name' => 'Arani',
            'code' => '030501',
            'province_id' => 19
        ]);
        Municipality::create([
            'name' => 'Vacas',
            'code' => '030502',
            'province_id' => 19
        ]);
        // esteban arce
        Municipality::create([
            'name' => 'Tarata',
            'code' => '030401',
            'province_id' => 20
        ]);
        Municipality::create([
            'name' => 'Anzaldo',
            'code' => '030402',
            'province_id' => 20
        ]);
        Municipality::create([
            'name' => 'Arbieto',
            'code' => '030403',
            'province_id' => 20
        ]);
        Municipality::create([
            'name' => 'Sacabamba',
            'code' => '030404',
            'province_id' => 20
        ]);
        // arque
        Municipality::create([
            'name' => 'Arque',
            'code' => '030601',
            'province_id' => 21
        ]);
        Municipality::create([
            'name' => 'Tacopaya',
            'code' => '030602',
            'province_id' => 21
        ]);
        // ayopaya
        Municipality::create([
            'name' => 'Independencia',
            'code' => '030301',
            'province_id' => 22
        ]);
        Municipality::create([
            'name' => 'Morochata',
            'code' => '030302',
            'province_id' => 22
        ]);
        Municipality::create([
            'name' => 'Cocapata',
            'code' => '030303',
            'province_id' => 22
        ]);
        // campero
        Municipality::create([
            'name' => 'Pasorapa',
            'code' => '030202',
            'province_id' => 23
        ]);
        Municipality::create([
            'name' => 'Aiquile',
            'code' => '030201',
            'province_id' => 23
        ]);
        Municipality::create([
            'name' => 'Omereque',
            'code' => '030203',
            'province_id' => 23
        ]);
        // capinota
        Municipality::create([
            'name' => 'Capinota',
            'code' => '030701',
            'province_id' => 24
        ]);
        Municipality::create([
            'name' => 'Sanatibañez',
            'code' => '030702',
            'province_id' => 24
        ]);
        Municipality::create([
            'name' => 'Sicaya',
            'code' => '030703',
            'province_id' => 24
        ]);
        // cercado
        Municipality::create([
            'name' => 'Cochabamba',
            'code' => '030101',
            'province_id' => 25
        ]);
        // carrasco
        Municipality::create([
            'name' => 'Pojo',
            'code' => '031202',
            'province_id' => 26
        ]);
        Municipality::create([
            'name' => 'Totora',
            'code' => '031201',
            'province_id' => 26
        ]);
        Municipality::create([
            'name' => 'Pocona',
            'code' => '031203',
            'province_id' => 26
        ]);
        Municipality::create([
            'name' => 'Chimoré',
            'code' => '031204',
            'province_id' => 26
        ]);
        Municipality::create([
            'name' => 'Puerto Villarroel',
            'code' => '031205',
            'province_id' => 26
        ]);
        Municipality::create([
            'name' => 'Bulo Bulo',
            'code' => '031206',
            'province_id' => 26
        ]);
        // chapare
        Municipality::create([
            'name' => 'Sacaba',
            'code' => '031001',
            'province_id' => 27
        ]);
        Municipality::create([
            'name' => 'Colomi',
            'code' => '031002',
            'province_id' => 27
        ]);
        Municipality::create([
            'name' => 'Villa Tunari',
            'code' => '031003',
            'province_id' => 27
        ]);
        // g. jordan
        Municipality::create([
            'name' => 'Cliza',
            'code' => '030801',
            'province_id' => 28
        ]);
        Municipality::create([
            'name' => 'Toco',
            'code' => '030802',
            'province_id' => 28
        ]);
        Municipality::create([
            'name' => 'Tolata',
            'code' => '030803',
            'province_id' => 28
        ]);
        // mizque
        Municipality::create([
            'name' => 'Mizque',
            'code' => '031301',
            'province_id' => 29
        ]);
        Municipality::create([
            'name' => 'Vila Vila',
            'code' => '031302',
            'province_id' => 29
        ]);
        Municipality::create([
            'name' => 'Alalay',
            'code' => '031303',
            'province_id' => 29
        ]);
        // punata
        Municipality::create([
            'name' => 'Punata',
            'code' => '031401',
            'province_id' => 30
        ]);
        Municipality::create([
            'name' => 'Villa Rivero',
            'code' => '031402',
            'province_id' => 30
        ]);
        Municipality::create([
            'name' => 'San Benito',
            'code' => '031403',
            'province_id' => 30
        ]);
        Municipality::create([
            'name' => 'Tacachi',
            'code' => '031404',
            'province_id' => 30
        ]);
        Municipality::create([
            'name' => 'Cuchumuela',
            'code' => '031405',
            'province_id' => 30
        ]);
        // quillacollo
        Municipality::create([
            'name' => 'Tiquipaya',
            'code' => '030903',
            'province_id' => 31
        ]);
        Municipality::create([
            'name' => 'Quillacollo',
            'code' => '030901',
            'province_id' => 31
        ]);
        Municipality::create([
            'name' => 'Sipe Sipe',
            'code' => '030902',
            'province_id' => 31
        ]);
        Municipality::create([
            'name' => 'Vinto',
            'code' => '030904',
            'province_id' => 31
        ]);
        Municipality::create([
            'name' => 'Colcapirhua',
            'code' => '030905',
            'province_id' => 31
        ]);
        // tapacarí
        Municipality::create([
            'name' => 'Tapacarí',
            'code' => '031101',
            'province_id' => 32
        ]);
        // bolivar
        Municipality::create([
            'name' => 'Bolivar',
            'code' => '031501',
            'province_id' => 33
        ]);
        // tiraque
        Municipality::create([
            'name' => 'Tiraque',
            'code' => '031601',
            'province_id' => 34
        ]);
        Municipality::create([
            'name' => 'Shinahota',
            'code' => '031602',
            'province_id' => 34
        ]);
        // *********** MUNICIPIOS LA PAZ
        // aroma
        Municipality::create([
            'name' => 'Umala',
            'code' => '021302',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Sica Sica',
            'code' => '021301',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Ayo Ayo',
            'code' => '021303',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Calamarca',
            'code' => '021304',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Patacamaya',
            'code' => '021305',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Colquencha',
            'code' => '021306',
            'province_id' => 35
        ]);
        Municipality::create([
            'name' => 'Collana',
            'code' => '021307',
            'province_id' => 35
        ]);
        // bautista saavedra
        Municipality::create([
            'name' => 'Charazani',
            'code' => '021601',
            'province_id' => 36
        ]);
        Municipality::create([
            'name' => 'Curva',
            'code' => '021602',
            'province_id' => 36
        ]);
        // abel iturralde
        Municipality::create([
            'name' => 'Ixiamas',
            'code' => '021501',
            'province_id' => 37
        ]);
        Municipality::create([
            'name' => 'San Buenaventura',
            'code' => '021502',
            'province_id' => 37
        ]);
        // caranavi
        Municipality::create([
            'name' => 'Caranavi',
            'code' => '022001',
            'province_id' => 38
        ]);
        Municipality::create([
            'name' => 'Alto Beni',
            'code' => '022002',
            'province_id' => 38
        ]);
        // camacho
        Municipality::create([
            'name' => 'Mocomoco',
            'code' => '020402',
            'province_id' => 39
        ]);
        Municipality::create([
            'name' => 'Puerto Acosta',
            'code' => '020401',
            'province_id' => 39
        ]);
        Municipality::create([
            'name' => 'Puerto Carabuco',
            'code' => '020403',
            'province_id' => 39
        ]);
        Municipality::create([
            'name' => 'Umanata',
            'code' => '020404',
            'province_id' => 39
        ]);
        Municipality::create([
            'name' => 'Escoma',
            'code' => '020405',
            'province_id' => 39
        ]);
        // franz tamayo
        Municipality::create([
            'name' => 'Apolo',
            'code' => '020701',
            'province_id' => 40
        ]);
        Municipality::create([
            'name' => 'Pelechuco',
            'code' => '020702',
            'province_id' => 40
        ]);
        // g. villarroel
        Municipality::create([
            'name' => 'San Pedro de Curahura',
            'code' => '021801',
            'province_id' => 41
        ]);
        Municipality::create([
            'name' => 'Papel Pampa',
            'code' => '021802',
            'province_id' => 41
        ]);
        Municipality::create([
            'name' => 'Chacarilla',
            'code' => '021803',
            'province_id' => 41
        ]);
        // ingavi
        Municipality::create([
            'name' => 'Viacha',
            'code' => '020801',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'Guaqui',
            'code' => '020802',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'Tihuanacu',
            'code' => '020803',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'Desaguadero',
            'code' => '020804',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'San Andrés de Machaca',
            'code' => '020805',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'Jesús de Machaca',
            'code' => '020806',
            'province_id' => 42
        ]);
        Municipality::create([
            'name' => 'Taraco',
            'code' => '020807',
            'province_id' => 42
        ]);
        // inquisivi
        Municipality::create([
            'name' => 'Inquisivi',
            'code' => '021001',
            'province_id' => 43
        ]);
        Municipality::create([
            'name' => 'Quime',
            'code' => '021002',
            'province_id' => 43
        ]);
        Municipality::create([
            'name' => 'Cajuata',
            'code' => '021003',
            'province_id' => 43
        ]);
        Municipality::create([
            'name' => 'Colquiri',
            'code' => '021004',
            'province_id' => 43
        ]);
        Municipality::create([
            'name' => 'Ichoca',
            'code' => '021005',
            'province_id' => 43
        ]);
        Municipality::create([
            'name' => 'Licoma Pampa',
            'code' => '021006',
            'province_id' => 43
        ]);
        // j. m. pando
        Municipality::create([
            'name' => 'Santiago de Machaca',
            'code' => '021901',
            'province_id' => 44
        ]);
        Municipality::create([
            'name' => 'Catacora',
            'code' => '021902',
            'province_id' => 44
        ]);
        // larecaja
        Municipality::create([
            'name' => 'Sorata',
            'code' => '020601',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Guanay',
            'code' => '020602',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Tacacoma',
            'code' => '020603',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Quiabaya',
            'code' => '020604',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Combaya',
            'code' => '020605',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Tipuani',
            'code' => '020606',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Mapiri',
            'code' => '020607',
            'province_id' => 45
        ]);
        Municipality::create([
            'name' => 'Teoponte',
            'code' => '020608',
            'province_id' => 45
        ]);
        // loayza
        Municipality::create([
            'name' => 'Luribay',
            'code' => '020901',
            'province_id' => 46
        ]);
        Municipality::create([
            'name' => 'Sapahaqui',
            'code' => '020902',
            'province_id' => 46
        ]);
        Municipality::create([
            'name' => 'Yaco',
            'code' => '020903',
            'province_id' => 46
        ]);
        Municipality::create([
            'name' => 'Malla',
            'code' => '020904',
            'province_id' => 46
        ]);
        Municipality::create([
            'name' => 'Cairoma',
            'code' => '020905',
            'province_id' => 46
        ]);
        // los andes
        Municipality::create([
            'name' => 'Pucarani',
            'code' => '021201',
            'province_id' => 47
        ]);
        Municipality::create([
            'name' => 'Laja',
            'code' => '021202',
            'province_id' => 47
        ]);
        Municipality::create([
            'name' => 'Batallas',
            'code' => '021203',
            'province_id' => 47
        ]);
        Municipality::create([
            'name' => 'Puerto Pérez',
            'code' => '021204',
            'province_id' => 47
        ]);
        // manco kapac
        Municipality::create([
            'name' => 'Copacabana',
            'code' => '021701',
            'province_id' => 48
        ]);
        Municipality::create([
            'name' => 'San Pedro de Tiquina',
            'code' => '021702',
            'province_id' => 48
        ]);
        Municipality::create([
            'name' => 'Tito Yupanqui',
            'code' => '021703',
            'province_id' => 48
        ]);
        // muñecas
        Municipality::create([
            'name' => 'Chuma',
            'code' => '020501',
            'province_id' => 49
        ]);
        Municipality::create([
            'name' => 'Ayata',
            'code' => '020502',
            'province_id' => 49
        ]);
        Municipality::create([
            'name' => 'Aucapata',
            'code' => '020503',
            'province_id' => 49
        ]);
        // nor yungas
        Municipality::create([
            'name' => 'Coroico',
            'code' => '021401',
            'province_id' => 50
        ]);
        Municipality::create([
            'name' => 'Coripata',
            'code' => '021402',
            'province_id' => 50
        ]);
        // omasuyus
        Municipality::create([
            'name' => 'Achacachi',
            'code' => '020201',
            'province_id' => 51
        ]);
        Municipality::create([
            'name' => 'Ancoraimes',
            'code' => '020202',
            'province_id' => 51
        ]);
        Municipality::create([
            'name' => 'Chua Cocani',
            'code' => '020203',
            'province_id' => 51
        ]);
        Municipality::create([
            'name' => 'Huarina',
            'code' => '020204',
            'province_id' => 51
        ]);
        Municipality::create([
            'name' => 'Santiago de Huata',
            'code' => '020205',
            'province_id' => 51
        ]);
        Municipality::create([
            'name' => 'Huatajata',
            'code' => '020206',
            'province_id' => 51
        ]);
        // pacajes
        Municipality::create([
            'name' => 'Comanche',
            'code' => '020304',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Coro Coro',
            'code' => '020301',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Caquiaviri',
            'code' => '020302',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Calacoto',
            'code' => '020303',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Charaña',
            'code' => '020305',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Waldo Ballivián',
            'code' => '020306',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Nazacara de Pacajes',
            'code' => '020307',
            'province_id' => 52
        ]);
        Municipality::create([
            'name' => 'Santiago de Callapa',
            'code' => '020308',
            'province_id' => 52
        ]);
        // murillo
        Municipality::create([
            'name' => 'La Paz',
            'code' => '020101',
            'province_id' => 53
        ]);
        Municipality::create([
            'name' => 'Palca',
            'code' => '020102',
            'province_id' => 53
        ]);
        Municipality::create([
            'name' => 'Mecapaca',
            'code' => '020103',
            'province_id' => 53
        ]);
        Municipality::create([
            'name' => 'Achocalla',
            'code' => '020104',
            'province_id' => 53
        ]);
        Municipality::create([
            'name' => 'El Alto',
            'code' => '020105',
            'province_id' => 53
        ]);
        // sud yungas
        Municipality::create([
            'name' => 'La Asunta',
            'code' => '021105',
            'province_id' => 54
        ]);
        Municipality::create([
            'name' => 'Chulumani',
            'code' => '021101',
            'province_id' => 54
        ]);
        Municipality::create([
            'name' => 'Irupana',
            'code' => '021102',
            'province_id' => 54
        ]);
        Municipality::create([
            'name' => 'Yanacachi',
            'code' => '021103',
            'province_id' => 54
        ]);
        // *********** MUNICIPIOS ORURO
        // sabaya
        Municipality::create([
            'name' => 'Sabaya',
            'code' => '040901',
            'province_id' => 55
        ]);
        Municipality::create([
            'name' => 'Coipasa',
            'code' => '040902',
            'province_id' => 55
        ]);
        Municipality::create([
            'name' => 'Chipaya',
            'code' => '040903',
            'province_id' => 55
        ]);
        // carangas
        Municipality::create([
            'name' => 'Choquecota',
            'code' => '040302',
            'province_id' => 56
        ]);
        Municipality::create([
            'name' => 'Corque',
            'code' => '040301',
            'province_id' => 56
        ]);
        // cercado
        Municipality::create([
            'name' => 'Oruro',
            'code' => '040101',
            'province_id' => 57
        ]);
        Municipality::create([
            'name' => 'Caracollo',
            'code' => '040102',
            'province_id' => 57
        ]);
        Municipality::create([
            'name' => 'El Choro',
            'code' => '040103',
            'province_id' => 57
        ]);
        Municipality::create([
            'name' => 'Soracachi',
            'code' => '040104',
            'province_id' => 57
        ]);
        // e. avaroa
        Municipality::create([
            'name' => 'Challapata',
            'code' => '040201',
            'province_id' => 58
        ]);
        Municipality::create([
            'name' => 'Santuario de Quillacas',
            'code' => '040202',
            'province_id' => 58
        ]);
        // l. cabrera
        Municipality::create([
            'name' => 'Salinas de Garci Mendoza',
            'code' => '040801',
            'province_id' => 59
        ]);
        Municipality::create([
            'name' => 'Pampa Aullagas',
            'code' => '040802',
            'province_id' => 59
        ]);
        // litoral
        Municipality::create([
            'name' => 'Esmeralda',
            'code' => '040505',
            'province_id' => 60
        ]);
        Municipality::create([
            'name' => 'Huachacalla',
            'code' => '040501',
            'province_id' => 60
        ]);
        Municipality::create([
            'name' => 'Escara',
            'code' => '040502',
            'province_id' => 60
        ]);
        Municipality::create([
            'name' => 'Cruz de Machacamarca',
            'code' => '040503',
            'province_id' => 60
        ]);
        Municipality::create([
            'name' => 'Yunguyo de Litoral',
            'code' => '040504',
            'province_id' => 60
        ]);
        // puerto mejillones
        Municipality::create([
            'name' => 'La Rivera',
            'code' => '041501',
            'province_id' => 61
        ]);
        Municipality::create([
            'name' => 'Todos Santos',
            'code' => '041502',
            'province_id' => 61
        ]);
        Municipality::create([
            'name' => 'Carangas',
            'code' => '041503',
            'province_id' => 61
        ]);
        // nor carangas
        Municipality::create([
            'name' => 'Santiago de Huayllamarca',
            'code' => '041601',
            'province_id' => 62
        ]);
        // pantaleon dalence
        Municipality::create([
            'name' => 'Villa Huanuni',
            'code' => '040701',
            'province_id' => 63
        ]);
        Municipality::create([
            'name' => 'Machacamarca',
            'code' => '040702',
            'province_id' => 63
        ]);
        // poopo
        Municipality::create([
            'name' => 'Villa Poopo',
            'code' => '040601',
            'province_id' => 64
        ]);
        Municipality::create([
            'name' => 'Pazña',
            'code' => '040602',
            'province_id' => 64
        ]);
        Municipality::create([
            'name' => 'Antequera',
            'code' => '040603',
            'province_id' => 64
        ]);
        // sajama
        Municipality::create([
            'name' => 'Curahuara de Carangas',
            'code' => '040401',
            'province_id' => 65
        ]);
        Municipality::create([
            'name' => 'Turco',
            'code' => '040402',
            'province_id' => 65
        ]);
        // san pedro de totora
        Municipality::create([
            'name' => 'Totora',
            'code' => '041301',
            'province_id' => 66
        ]);
        // saucari
        Municipality::create([
            'name' => 'Toledo',
            'code' => '041001',
            'province_id' => 67
        ]);
        // sebastian pagador
        Municipality::create([
            'name' => 'Santiago de Huari',
            'code' => '041401',
            'province_id' => 68
        ]);
        // sud carangas
        Municipality::create([
            'name' => 'Andamarca',
            'code' => '041201',
            'province_id' => 69
        ]);
        Municipality::create([
            'name' => 'Eucaliptus',
            'code' => '041101',
            'province_id' => 70
        ]);
        // *********** MUNICIPIOS PANDO
        // abuná
        Municipality::create([
            'name' => 'Santa Rosa de Abuná',
            'code' => '090401',
            'province_id' => 71
        ]);
        Municipality::create([
            'name' => 'Humaita',
            'code' => '090402',
            'province_id' => 71
        ]);
        // federico roman
        Municipality::create([
            'name' => 'Nueva Esperanza',
            'code' => '090501',
            'province_id' => 72
        ]);
        Municipality::create([
            'name' => 'Villa Nueva',
            'code' => '090502',
            'province_id' => 72
        ]);
        Municipality::create([
            'name' => 'Santos Mercado',
            'code' => '090503',
            'province_id' => 72
        ]);
        // madre de dios
        Municipality::create([
            'name' => 'Pto. Gonzalo Moreno',
            'code' => '090301',
            'province_id' => 73
        ]);
        Municipality::create([
            'name' => 'San Lorenzo',
            'code' => '090302',
            'province_id' => 73
        ]);
        Municipality::create([
            'name' => 'El Sena',
            'code' => '090303',
            'province_id' => 73
        ]);
        // manuripi
        Municipality::create([
            'name' => 'Puerto Rico',
            'code' => '090201',
            'province_id' => 74
        ]);
        Municipality::create([
            'name' => 'San Pedro',
            'code' => '090202',
            'province_id' => 74
        ]);
        Municipality::create([
            'name' => 'Filadelfia',
            'code' => '090203',
            'province_id' => 74
        ]);
        // nicolas suarez
        Municipality::create([
            'name' => 'Cobija',
            'code' => '090101',
            'province_id' => 75
        ]);
        Municipality::create([
            'name' => 'Porvenir',
            'code' => '090102',
            'province_id' => 75
        ]);
        Municipality::create([
            'name' => 'Bolpebra',
            'code' => '090103',
            'province_id' => 75
        ]);
        Municipality::create([
            'name' => 'Bella Flor',
            'code' => '090104',
            'province_id' => 75
        ]);
        // *********** MUNICIPIOS POTOSI
        // alonso de ibañez
        Municipality::create([
            'name' => 'Sacaca',
            'code' => '050701',
            'province_id' => 76
        ]);
        Municipality::create([
            'name' => 'Caripuyo',
            'code' => '050702',
            'province_id' => 76
        ]);
        // antonio quijarro
        Municipality::create([
            'name' => 'Uyuni',
            'code' => '051201',
            'province_id' => 77
        ]);
        Municipality::create([
            'name' => 'Tomave',
            'code' => '051202',
            'province_id' => 77
        ]);
        Municipality::create([
            'name' => 'Porco',
            'code' => '051203',
            'province_id' => 77
        ]);
        // bernardino bilbao
        Municipality::create([
            'name' => 'Arampampa',
            'code' => '051301',
            'province_id' => 78
        ]);
        Municipality::create([
            'name' => 'Acasio',
            'code' => '051302',
            'province_id' => 78
        ]);
        // charcas
        Municipality::create([
            'name' => 'San Pedro de Buena Vista',
            'code' => '050501',
            'province_id' => 79
        ]);
        Municipality::create([
            'name' => 'Toro Toro',
            'code' => '050502',
            'province_id' => 79
        ]);
        // chayanta
        Municipality::create([
            'name' => 'Colquechaca',
            'code' => '050401',
            'province_id' => 80
        ]);
        Municipality::create([
            'name' => 'Ravelo',
            'code' => '050402',
            'province_id' => 80
        ]);
        Municipality::create([
            'name' => 'Pocoata',
            'code' => '050403',
            'province_id' => 80
        ]);
        Municipality::create([
            'name' => 'Ucurí',
            'code' => '050404',
            'province_id' => 80
        ]);
        // cornelio saavedra
        Municipality::create([
            'name' => 'Chaquí',
            'code' => '050302',
            'province_id' => 81
        ]);
        Municipality::create([
            'name' => 'Betanzos',
            'code' => '050301',
            'province_id' => 81
        ]);
        Municipality::create([
            'name' => 'Tacobamba',
            'code' => '050303',
            'province_id' => 81
        ]);
        // daniel campos
        Municipality::create([
            'name' => 'Llica',
            'code' => '051401',
            'province_id' => 82
        ]);
        Municipality::create([
            'name' => 'Tahua',
            'code' => '051402',
            'province_id' => 82
        ]);
        // enrique baldivieso
        Municipality::create([
            'name' => 'San Agustín',
            'code' => '051601',
            'province_id' => 83
        ]);
        // jose maria linares
        Municipality::create([
            'name' => 'Ckochas',
            'code' => '051103',
            'province_id' => 84
        ]);
        Municipality::create([
            'name' => 'Puna',
            'code' => '051101',
            'province_id' => 84
        ]);
        Municipality::create([
            'name' => 'Caiza',
            'code' => '051102',
            'province_id' => 84
        ]);
        // Modesto omiste
        Municipality::create([
            'name' => 'Villazón',
            'code' => '051501',
            'province_id' => 85
        ]);
        // nor chichas
        Municipality::create([
            'name' => 'Cotagaita',
            'code' => '050601',
            'province_id' => 86
        ]);
        Municipality::create([
            'name' => 'Vitichi',
            'code' => '050602',
            'province_id' => 86
        ]);
        // nor lipez
        Municipality::create([
            'name' => 'Colcha',
            'code' => '050901',
            'province_id' => 87
        ]);
        Municipality::create([
            'name' => 'San Pedro de Quemes',
            'code' => '050902',
            'province_id' => 87
        ]);
        // rafael bustillo
        Municipality::create([
            'name' => 'Uncia',
            'code' => '050201',
            'province_id' => 88
        ]);
        Municipality::create([
            'name' => 'Chayanta',
            'code' => '050202',
            'province_id' => 88
        ]);
        Municipality::create([
            'name' => 'Llallagua',
            'code' => '050203',
            'province_id' => 88
        ]);
        Municipality::create([
            'name' => 'Chuquihuta Ayllu Jucumani',
            'code' => '050204',
            'province_id' => 88
        ]);
        // sud chichas
        Municipality::create([
            'name' => 'Tupiza',
            'code' => '050801',
            'province_id' => 89
        ]);
        Municipality::create([
            'name' => 'Atocha',
            'code' => '050802',
            'province_id' => 89
        ]);
        // sud lipez
        Municipality::create([
            'name' => 'San Pablo de Lipez',
            'code' => '051001',
            'province_id' => 90
        ]);
        Municipality::create([
            'name' => 'Mojinete',
            'code' => '051002',
            'province_id' => 90
        ]);
        Municipality::create([
            'name' => 'San Antonio de Esmoruco',
            'code' => '051003',
            'province_id' => 90
        ]);
        // tomas frias
        Municipality::create([
            'name' => 'Potosí',
            'code' => '050101',
            'province_id' => 91
        ]);
        Municipality::create([
            'name' => 'Tinguipaya',
            'code' => '050102',
            'province_id' => 91
        ]);
        Municipality::create([
            'name' => 'Villa de Yocalla',
            'code' => '050103',
            'province_id' => 91
        ]);
        Municipality::create([
            'name' => 'Belén de Urmiri',
            'code' => '050104',
            'province_id' => 91
        ]);
        // *********** MUNICIPIOS SANTA CRUZ
        // andres ibañez
        Municipality::create([
            'name' => 'Santa Cruz de la Sierra',
            'code' => '070101',
            'province_id' => 92
        ]);
        Municipality::create([
            'name' => 'Cotoca',
            'code' => '070102',
            'province_id' => 92
        ]);
        Municipality::create([
            'name' => 'Porongo',
            'code' => '070103',
            'province_id' => 92
        ]);
        Municipality::create([
            'name' => 'La Guardia',
            'code' => '070104',
            'province_id' => 92
        ]);
        Municipality::create([
            'name' => 'El Torno',
            'code' => '070105',
            'province_id' => 92
        ]);
        // warnes
        Municipality::create([
            'name' => 'Warnes',
            'code' => '070201',
            'province_id' => 93
        ]);
        Municipality::create([
            'name' => 'Okinawa',
            'code' => '070202',
            'province_id' => 93
        ]);
        // jose miguel velasco
        Municipality::create([
            'name' => 'San Ignacio',
            'code' => '070301',
            'province_id' => 94
        ]);
        Municipality::create([
            'name' => 'San Miguel',
            'code' => '070302',
            'province_id' => 94
        ]);
        Municipality::create([
            'name' => 'San Rafael',
            'code' => '070303',
            'province_id' => 94
        ]);
        // ichilo
        Municipality::create([
            'name' => 'Buena Vista',
            'code' => '070401',
            'province_id' => 95
        ]);
        Municipality::create([
            'name' => 'San Carlos',
            'code' => '070402',
            'province_id' => 95
        ]);
        Municipality::create([
            'name' => 'Yapacaní',
            'code' => '070403',
            'province_id' => 95
        ]);
        Municipality::create([
            'name' => 'San Juan',
            'code' => '070404',
            'province_id' => 95
        ]);
        // chiquitos
        Municipality::create([
            'name' => 'San José',
            'code' => '070501',
            'province_id' => 96
        ]);
        Municipality::create([
            'name' => 'Pailón',
            'code' => '070502',
            'province_id' => 96
        ]);
        Municipality::create([
            'name' => 'Roboré',
            'code' => '070503',
            'province_id' => 96
        ]);
        // sara
        Municipality::create([
            'name' => 'Portachuelo',
            'code' => '070601',
            'province_id' => 97
        ]);
        Municipality::create([
            'name' => 'Santa Rosa de Sara',
            'code' => '070602',
            'province_id' => 97
        ]);
        Municipality::create([
            'name' => 'Colpa Bélgica',
            'code' => '070603',
            'province_id' => 97
        ]);
        // cordillera
        Municipality::create([
            'name' => 'Lagunillas',
            'code' => '070701',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Charaguas',
            'code' => '070702',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Cabezas',
            'code' => '070703',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Cuevo',
            'code' => '070704',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Gutiérrez',
            'code' => '070705',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Camiri',
            'code' => '070706',
            'province_id' => 98
        ]);
        Municipality::create([
            'name' => 'Boyuibe',
            'code' => '070707',
            'province_id' => 98
        ]);
        // vallegrande
        Municipality::create([
            'name' => 'Vallegrande',
            'code' => '070801',
            'province_id' => 99
        ]);
        Municipality::create([
            'name' => 'El Trigal',
            'code' => '070802',
            'province_id' => 99
        ]);
        Municipality::create([
            'name' => 'Moro Moro',
            'code' => '070803',
            'province_id' => 99
        ]);
        Municipality::create([
            'name' => 'Postrervalle',
            'code' => '070804',
            'province_id' => 99
        ]);
        Municipality::create([
            'name' => 'Pucará',
            'code' => '070805',
            'province_id' => 99
        ]);
        // florida
        Municipality::create([
            'name' => 'Samaipata',
            'code' => '070901',
            'province_id' => 100
        ]);
        Municipality::create([
            'name' => 'Pampa Grande',
            'code' => '070902',
            'province_id' => 100
        ]);
        Municipality::create([
            'name' => 'Mairana',
            'code' => '070903',
            'province_id' => 100
        ]);
        Municipality::create([
            'name' => 'Quirusillas',
            'code' => '070904',
            'province_id' => 100
        ]);
        // santiesteban
        Municipality::create([
            'name' => 'Montero',
            'code' => '071001',
            'province_id' => 101
        ]);
        Municipality::create([
            'name' => 'Gral. Saavedra',
            'code' => '071002',
            'province_id' => 101
        ]);
        Municipality::create([
            'name' => 'Puerto Fernandez Alonso',
            'code' => '071004',
            'province_id' => 101
        ]);
        Municipality::create([
            'name' => 'San Pedro',
            'code' => '071005',
            'province_id' => 101
        ]);
        // ñuflo de chavez
        Municipality::create([
            'name' => 'Concepción',
            'code' => '071101',
            'province_id' => 102
        ]);
        Municipality::create([
            'name' => 'San Javier',
            'code' => '071102',
            'province_id' => 102
        ]);
        Municipality::create([
            'name' => 'San Ramón',
            'code' => '071103',
            'province_id' => 102
        ]);
        Municipality::create([
            'name' => 'San Julián',
            'code' => '071104',
            'province_id' => 102
        ]);
        Municipality::create([
            'name' => 'San Antonio de Lomerío',
            'code' => '071105',
            'province_id' => 102
        ]);
        Municipality::create([
            'name' => 'Cuatro Cañadas',
            'code' => '071106',
            'province_id' => 102
        ]);
        // angel sandoval
        Municipality::create([
            'name' => 'San Matías',
            'code' => '071201',
            'province_id' => 103
        ]);
        // caballero
        Municipality::create([
            'name' => 'Comarapa',
            'code' => '071301',
            'province_id' => 104
        ]);
        Municipality::create([
            'name' => 'Saipina',
            'code' => '071302',
            'province_id' => 104
        ]);
        // german busch
        Municipality::create([
            'name' => 'Puerto Suárez',
            'code' => '071401',
            'province_id' => 105
        ]);
        Municipality::create([
            'name' => 'Pueto Quijarro',
            'code' => '071402',
            'province_id' => 105
        ]);
        Municipality::create([
            'name' => 'Carmen Rivero Torres',
            'code' => '071403',
            'province_id' => 105
        ]);
        // Guarayos
        Municipality::create([
            'name' => 'Ascención de Guarayos',
            'code' => '071501',
            'province_id' => 106
        ]);
        Municipality::create([
            'name' => 'Urubichá',
            'code' => '071502',
            'province_id' => 106
        ]);
        Municipality::create([
            'name' => 'El Puente',
            'code' => '071503',
            'province_id' => 106
        ]);
        // *********** MUNICIPIOS TARIJA
        // aniceto arce
        Municipality::create([
            'name' => 'Bermejo',
            'code' => '060202',
            'province_id' => 107
        ]);
        Municipality::create([
            'name' => 'Padcaya',
            'code' => '060201',
            'province_id' => 107
        ]);
        // o'connor
        Municipality::create([
            'name' => 'Entre Ríos',
            'code' => '060601',
            'province_id' => 108
        ]);
        // cercado
        Municipality::create([
            'name' => 'Tarija',
            'code' => '060101',
            'province_id' => 109
        ]);
        // eustaquio mendez
        Municipality::create([
            'name' => 'San Lorenzo',
            'code' => '060501',
            'province_id' => 110
        ]);
        Municipality::create([
            'name' => 'El Puente',
            'code' => '060502',
            'province_id' => 110
        ]);
        // gran chaco
        Municipality::create([
            'name' => 'Yacuiba',
            'code' => '060301',
            'province_id' => 111
        ]);
        Municipality::create([
            'name' => 'Caraparí',
            'code' => '060302',
            'province_id' => 111
        ]);
        Municipality::create([
            'name' => 'Villamontes',
            'code' => '060303',
            'province_id' => 111
        ]);
        // jose maria avilez
        Municipality::create([
            'name' => 'Concepción',
            'code' => '060401',
            'province_id' => 112
        ]);
        Municipality::create([
            'name' => 'Yunchará',
            'code' => '060402',
            'province_id' => 112
        ]);
    }
}
